using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SzPEK.Models
{
    public class CourseParticipatesViewModel
    {
        public int CourseID { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        
        [Display(Name = "Starting Date")]
        [DataType(DataType.Date)]
        public DateTime StartingDate { get; set; }
       
        public IList<CourseParticipate> CourseParticipates {get; set;} 
        public List<Participate> Participates {get; set;}
    } 
}