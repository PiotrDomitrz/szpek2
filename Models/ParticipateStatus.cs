namespace SzPEK.Models
{
    public enum ParticipateStatus
    {
        WAITING,
        CONFIRMED,
        REJECTED
    }
}