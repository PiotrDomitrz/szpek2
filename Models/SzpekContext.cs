using Microsoft.EntityFrameworkCore;

namespace SzPEK.Models
{
    public class SzpekContext : DbContext
    {
        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        // {
        //     optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=EFCore-SchoolDB;Trusted_Connection=True");
        // }
        public SzpekContext (DbContextOptions<SzpekContext> options)
            : base(options)
        {
        }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Participate> Participates { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CourseParticipate>()
                .HasKey(sc => new { sc.CourseID, sc.ParticipateID });

            modelBuilder.Entity<CourseParticipate>()
                .HasOne<Course>(cp => cp.Course)
                .WithMany(c => c.CourseParticipates)
                .HasForeignKey(cp => cp.CourseID)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CourseParticipate>()
                .HasOne<Participate>(cp => cp.Participate)
                .WithMany(c => c.CourseParticipates)
                .HasForeignKey(cp => cp.ParticipateID)
                .OnDelete(DeleteBehavior.Cascade);
        }
        
    }
}