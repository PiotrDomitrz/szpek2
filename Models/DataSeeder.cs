using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Collections.Generic;

namespace SzPEK.Models
{
    public static class DataSeeder
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new SzpekContext(
                serviceProvider.GetRequiredService<DbContextOptions<SzpekContext>>()))
            {
                if (context.Courses.Any())
                {
                    return;
                }
                
                var course = new Course
                {
                    Subject = "Depresja",
                    Description = "Czy w Polsce można powtórzyć sukces japońskich psychologów?",
                    StartingDate = new DateTime(2018,11,10)
                };
                var participate = new Participate
                {
                    Name = "Andrzej",
                    Email = "a@vp.pl",
                    Status = ParticipateStatus.WAITING.ToString()
                };

                course.CourseParticipates = new List<CourseParticipate>
                {
                    new CourseParticipate
                    {
                        Course = course,
                        Participate = participate
                    }
                };
                context.Courses.Add(course);
                context.SaveChanges();
            }
        }
    }
}
