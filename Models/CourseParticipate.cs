using System;

namespace SzPEK.Models
{
    public class CourseParticipate
    {
        public int CourseID {get; set;}
        public Course Course {get; set;}
        
        public int ParticipateID {get; set;}
        public Participate Participate {get; set;}
    } 
}