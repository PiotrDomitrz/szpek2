using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SzPEK.Models;
using Microsoft.EntityFrameworkCore;

namespace SzPEK.Controllers
{

    public class CoursesController : Controller
    {
        private readonly SzpekContext _context;
                       
        public CoursesController(SzpekContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var model = await GetCoursesAsync();
            return View(model);
        }
        
        public async Task<IActionResult> Details(int? id)
        {
            var model = new CourseParticipatesViewModel();
            var course = await GetCourseByIdAsync(id);

            if(course == null || id==null)
            {
                return NotFound();
            }

            var participates = await GetParticipatesByCourseIdAsync(id);
            SetDataInCourseParticipatesViewModel(course, participates, model);
            return View(model);
        }

        public async Task<IActionResult> DeleteCourse(int? id)
        {
            var model = await GetParticipateByIdAsync(id);
            if(model == null)
            {
                return NotFound();
            }
            return View(model);
        }

        [HttpPost, ActionName("DeleteCourse")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteCourseConfirmed(int id)
        { 
            var course = await GetCourseByIdAsync(id);
            await RemoveCourseFromDataBaseAsync(course);
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if(id == null)
            {
                return NotFound();
            }
            
            var model = await GetCourseByIdAsync(id);
            
            if(model == null)
            {
                return NotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Course course)
        {
            await UpdateCourseInContextAsync(course);
            return View(course);
        }

        public async Task<IActionResult> Accept(int? courseId, int? participateId)
        {
            var participate=await GetParticipateByIdAsync(participateId);
            if(participate == null)
            {
                return NotFound();
            }

            var model = new ParticipateCourseIdViewModel();
            SetDataInParticipateCourseIdViewModel(participate, courseId, model);
            
            return View(model);
        }

        [HttpPost, ActionName("Accept")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AcceptConfirmed(int courseId, Participate participate)
        { 
            var participateModel=await GetParticipateByIdAsync(participate.ParticipateID); 
            participateModel.Status=ParticipateStatus.CONFIRMED.ToString();
            await UpdateParticipateInContextAsync(participateModel);
            return RedirectToAction(nameof(Details), new {id = courseId});
        }

        public async Task<IActionResult> Reject(int? courseId, int? participateId)
        {
            var participate = await GetParticipateByIdAsync(participateId);
            if(participate == null)
            {
                return NotFound();
            }

            var model = new ParticipateCourseIdViewModel();
            SetDataInParticipateCourseIdViewModel(participate, courseId, model);
            
            return View(model);
        }

        [HttpPost, ActionName("Reject")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RejectConfirmed(int courseId, Participate participate)
        { 
            var participateModel=await GetParticipateByIdAsync(participate.ParticipateID);
            participateModel.Status=ParticipateStatus.REJECTED.ToString();
            await UpdateParticipateInContextAsync(participateModel);
            return RedirectToAction(nameof(Details), new {id = courseId});
        }

        public async Task<IActionResult> Delete(int? courseId, int? participateId)
        {
            var participate=await GetParticipateByIdAsync(participateId);
            if(participate == null)
            {
                return NotFound();
            }

            var model = new ParticipateCourseIdViewModel();
            SetDataInParticipateCourseIdViewModel(participate, courseId, model);

            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int courseId, Participate participate)
        {
            var participateModel=await GetParticipateByIdAsync(participate.ParticipateID);
            await RemoveParticipateFromDataBaseAsync(participateModel);
            return RedirectToAction(nameof(Details), new {id = courseId});
        }

        private async Task<List<Course>> GetCoursesAsync()
        {
           return await _context.Courses.ToListAsync();
        }

        private async Task<Course> GetCourseByIdAsync(int? id)
        {
            return await _context.Courses.SingleOrDefaultAsync(m => m.CourseID == id);
        }

        private async Task UpdateCourseInContextAsync(Course course)
        {
            _context.Update(course);
            await _context.SaveChangesAsync();
        }
        private async Task UpdateParticipateInContextAsync(Participate participate)
        {
            _context.Update(participate);
            await _context.SaveChangesAsync();
        }

        private async Task RemoveCourseFromDataBaseAsync(Course course)
        {
            _context.Courses.Remove(course);
            await _context.SaveChangesAsync();
        }

        private void SetDataInCourseParticipatesViewModel(Course course, List<Participate> participates, CourseParticipatesViewModel model)
        {
            model.Subject = course.Subject;
            model.Description = course.Description;
            model.StartingDate = course.StartingDate;
            model.CourseParticipates = course.CourseParticipates;
            model.CourseID = course.CourseID;
            model.Participates = participates;
        }

        private void SetDataInParticipateCourseIdViewModel(Participate participate, int? CourseID, ParticipateCourseIdViewModel model)
        {
            model.CourseID = CourseID.Value;
            model.ParticipateID = participate.ParticipateID;
            model.Email = participate.Email;
            model.Name = participate.Name;
            model.Status = participate.Status;
            model.CourseParticipates = participate.CourseParticipates;
        }

        private async Task<List<Participate>> GetParticipatesByCourseIdAsync(int? id)
        {
            return await _context.Participates
                .Where(x => x.CourseParticipates
                .Any(y => y.CourseID==id))
                .ToListAsync();
        }

        private async Task<Participate> GetParticipateByIdAsync(int? id)
        {
            return await _context.Participates.SingleOrDefaultAsync(m => m.ParticipateID == id);
        }
        
        private async Task RemoveParticipateFromDataBaseAsync(Participate participate)
        {
            _context.Participates.Remove(participate);
            await _context.SaveChangesAsync();
        }
    }
}