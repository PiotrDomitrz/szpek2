﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Configuration;
using SzPEK.Models;
using System.Linq;
using System;
using Npgsql;

namespace SzPEK
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // services.AddEntityFrameworkInMemoryDatabase()
            //         .AddDbContext<SzpekContext>((serviceProvider, options) =>
            //             options.UseInMemoryDatabase("InMemoryDbForTesting")
            //                 .UseInternalServiceProvider(serviceProvider));
            
            // NpgsqlConnection.GlobalTypeMapper.MapEnum<ParticipateStatus>("ParticipateStatus");
            
            string connectionString = Configuration["ConnectionString"];
            services.AddDbContext<SzpekContext>(options =>
                    {
                        options.UseNpgsql(connectionString);
                    });
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            SetExceptionSources(app, env);
            app.UseStaticFiles();
            SetMapRoute(app);  
        }

        private void SetExceptionSources(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
        }

        private void SetMapRoute(IApplicationBuilder app)
        {
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Courses}/{action=Index}/{id?}");
            });
        }
    }
}
